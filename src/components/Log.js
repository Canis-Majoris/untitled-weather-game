import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import { List, ListItem, ListItemText, Chip } from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';
import DoneIcon from '@material-ui/icons/Done';

const useStyles = makeStyles({
    bullet: {
        display: 'inline-block',
        margin: '0 2px',
        transform: 'scale(0.8)',
    },
    title: {
        fontSize: 14,
    },
    pos: {
        marginBottom: 12,
    },
})

const Log = props => {
    const classes = useStyles();

    const {
        log
    } = props;

    return (
        <div className="log-wrapper">
            <Typography className={classes.title} color="textSecondary" gutterBottom>
                Log
            </Typography>
            <div className="log-list-wrapper">
                <List className={'log-list'}>
                    {log.length ? log.map((l, i) => (
                        <ListItem key={`log-item-${i}`} className="log-item">
                            <div className="log-item-result">
                                <span>{l.result ? <DoneIcon className="c-correct" /> : <CloseIcon className="c-wrong" />}</span>
                                <ListItemText style={{ marginLeft: '1rem' }} primary={l.result ? 'Correct' : 'Wrong'} secondary={l.date} />
                            </div>
                            <div className="log-item-content">
                                {l.cities.map((lc, j) =>
                                    <Chip
                                        className={`log-chip ${l.result
                                            ? l.correctAnswerIndex === j ? 'correct' : ''
                                            : l.wrongAnswerIndex === j ? 'wrong' : l.correctAnswerIndex === j ?  'correct' : ''}`}
                                        key={`log-city-${i}-${lc.name}`}
                                        label={lc.name}
                                    />)
                                }
                            </div>
                        </ListItem>
                    )) : (
                        <Typography>Log is Empty</Typography>
                    )}
                </List>
            </div>
        </div>
    );
}

export default Log ;