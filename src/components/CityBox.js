import React, { useState, useEffect } from 'react';
import Button from '@material-ui/core/Button';
import Skeleton from '@material-ui/lab/Skeleton';

const CityBox = props => {
    const { 
        city, 
        units,
        onCityClick, 
        currentPlayStage,
        showResults, 
        correctAnswerIndex,
        fetchCitiesPending
    } = props;

    const [loading, setLoading] = useState(fetchCitiesPending)

    useEffect(() => {
        city && city.name && setTimeout(e => {
            setLoading(false)
        }, 250);
    }, [city])

    useEffect(() => {
        setLoading(fetchCitiesPending)
    }, [fetchCitiesPending])

    const tempUnitConverter = (temp) => {
        return units === 'metric' ? temp : (temp * 9 / 5 + 32).toFixed(2);
    }

    return (
        <div className={`city-box ${loading ? 'loading' : ''}`}>
            {!loading ? (
                <Button 
                    variant="outlined" 
                    color="primary" 
                    onClick={onCityClick} disabled={currentPlayStage === 'result'} 
                    className={`city-box-button ${showResults ? (correctAnswerIndex ? 'correct' : 'incorrect') : ''}`}
                    classes={{label: 'city-box-button-label'}}
                >
                    <div className="title">{city.name}</div>
                    {showResults && (
                        <div className="temp">{units === 'metric' ? 'C' : 'F'} {tempUnitConverter(city.main.temp)}</div>
                    )}
                </Button>
            ) : (
                <Skeleton variant="rect" width={200} height={64} />
            )}
        </div>
    );
}

export default CityBox
