import React, { useEffect, useRef, useState } from 'react';
import { connect } from 'react-redux';
import {
    BrowserRouter as Router,

    Route, Switch
} from "react-router-dom";
import cities from '../assets/city.list.json';
import { fetchCityData, setCorrectAnswerIndex, setFeedbackMessage, setFeedbackStatus, setPlayStage, setPlayStatus, updateLog, updatePoints } from '../store/actions/game.js';
import Feedback from './Feedback';
import Header from './Header';
import PlayPanel from './PlayPanel';
import Settings from './Settings';

const Game = props => {
    const {
        onFetchCityData,
        onSetPlayStatus,
        onSetPlayStage,
        onSetCorrectAnswerIndex,
        feedbackOpened,
        feedbackMessage,
        citiesNumber,
        onSetFeedbackMessage,
        onUpdateLog,
        onUpdatePoints
    } = props;

    const [currentCityes, setCurrentCities] = useState([]);

    useEffect(() => {
        showNextPlay()
    }, [citiesNumber])

    useEffect(() => {
        if (currentCityes.length === citiesNumber) {
            fetchCityWeatherData()
        }
    }, [currentCityes])

    const fetchCityWeatherData = () => {
        onFetchCityData(currentCityes)
    }

    const retry = () => {
        setCities()
        onSetPlayStage('play')
        onSetPlayStatus(false)
        onSetCorrectAnswerIndex(-1)
        onSetFeedbackMessage(null)
        onUpdateLog([])
        onUpdatePoints({
            overall: 0,
            correct: 0
        })
        localStorage.clear();
    }

    const showNextPlay = () => {
        setCities()
        onSetPlayStage('play')
        onSetPlayStatus(false)
        onSetCorrectAnswerIndex(-1)
        onSetFeedbackMessage(null)
    }

    const setCities = () => {
        const citiesArr = [];
        for (let i = 0; i < citiesNumber; i++) {
            let selectedeCity = null;
            let isInList = 0;

            while (isInList !== -1 && selectedeCity === null) {
                selectedeCity = cities[Math.floor(Math.random() * cities.length)];
                selectedeCity && (isInList = citiesArr.findIndex(c => c.id === selectedeCity.id));
            }

            citiesArr.push(selectedeCity);
        }

        setCurrentCities(citiesArr);
    }

    return (
        <Router>
            <div className="App">
                <Header />
                <Switch>
                    <Route path="/settings">
                        <Settings />
                    </Route>
                    <Route path="/">
                        <PlayPanel showNextPlay={showNextPlay} retry={retry}/>
                    </Route>
                </Switch>
                <Feedback opened={feedbackOpened} message={feedbackMessage}/>
            </div>
        </Router>
    );
}

const mapStateToProps = ({ gameReducer }) => {
    const { currentPlayStatus, units, feedbackOpened, feedbackMessage, citiesNumber } = gameReducer;
    return { currentPlayStatus, units, feedbackOpened, feedbackMessage, citiesNumber }
};

const mapDispatchToProps = dispatch => {
    return {
        onFetchCityData: cityArr => dispatch(fetchCityData(cityArr)),
        onSetPlayStatus: status => dispatch(setPlayStatus(status)),
        onSetPlayStage: stage => dispatch(setPlayStage(stage)),
        onSetCorrectAnswerIndex: index => dispatch(setCorrectAnswerIndex(index)),
        onSetFeedbackStatus: status => dispatch(setFeedbackStatus(status)),
        onSetFeedbackMessage: message => dispatch(setFeedbackMessage(message)),
        onUpdateLog: log => dispatch(updateLog(log)),
        onUpdatePoints: points => dispatch(updatePoints(points)),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Game);