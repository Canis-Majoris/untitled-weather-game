import React from 'react';
import { Container } from '@material-ui/core';

const Header = props => {
    return (
        <Container maxWidth="lg">
            <div className="header">
                <h1 className="title">Untitled Weather Game</h1>
            </div>
        </Container>
    );
}

export default Header
