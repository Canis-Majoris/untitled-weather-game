import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import CityBox from './CityBox';
import { setPlayStatus, setPlayStage, setCorrectAnswerIndex, updateLog, updatePoints, setFeedbackStatus, setFeedbackMessage, afterCompareCityTemp } from '../store/actions/game';

const CityCouple = props => {
    const {
        cities,
        units,
        onSetPlayStatus,
        onSetPlayStage,
        onSetCorrectAnswerIndex,
        currentPlayStage,
        correctAnswerIndex,
        fetchCitiesPending,
        onSetFeedbackStatus,
        onSetFeedbackMessage,
        onAfterCompareCityTemp
    } = props;

    const [showResults, setShowResults] = useState(false)

    useEffect(() => {
        if (currentPlayStage === 'result' && correctAnswerIndex >= 0) {
            setShowResults(true)
        } else if (currentPlayStage === 'play') {
            setShowResults(false)
        }
    }, [correctAnswerIndex, currentPlayStage])

    const onCityClick = index => {
        onSetPlayStage('result')
        onSetPlayStatus(compareCityTemp(index));
    }

    const triggerFeedback = (result) => {
        onSetFeedbackMessage(result ? 'Correct! you get one more point' : 'Nope... try the next one!')
        onSetFeedbackStatus(result ? 'success' : 'error')
    }

    const compareCityTemp = index => {
        const selectedCity = cities[index];
        const currentHighetsTemp = selectedCity.main.temp;
        let currentIndex = index;
        cities.forEach((city, ci) => {
            if (city.main.temp > currentHighetsTemp) currentIndex = ci;
        });

        const result = currentIndex === index;

        onSetCorrectAnswerIndex(currentIndex);
        onAfterCompareCityTemp({index, currentIndex})
        triggerFeedback(result)

        return result;
    }
    

    return (
        <div className="city-couple">
            {cities.map((city, i) =>
                <CityBox
                    units={units}
                    showResults={showResults}
                    fetchCitiesPending={fetchCitiesPending}
                    correctAnswerIndex={correctAnswerIndex === i}
                    key={`city-box-${i}`}
                    currentPlayStage={currentPlayStage}
                    city={city}
                    onCityClick={e => onCityClick(i)} />
            )}
        </div>
    );
}

const mapStateToProps = ({ gameReducer }) => {
    const { units, cities, log, points, currentPlayStage, correctAnswerIndex, fetchCitiesPending, onafterprintCompareCityTemp } = gameReducer;
    return { units, cities, log, points, currentPlayStage, correctAnswerIndex, fetchCitiesPending, onafterprintCompareCityTemp }
};
const mapDispatchToProps = dispatch => {
    return {
        onSetPlayStatus: status => dispatch(setPlayStatus(status)),
        onSetPlayStage: stage => dispatch(setPlayStage(stage)),
        onSetCorrectAnswerIndex: index => dispatch(setCorrectAnswerIndex(index)),
        onUpdateLog: log => dispatch(updateLog(log)),
        onUpdatePoints: points => dispatch(updatePoints(points)),
        onSetFeedbackStatus: status => dispatch(setFeedbackStatus(status)),
        onSetFeedbackMessage: message => dispatch(setFeedbackMessage(message)),
        onAfterCompareCityTemp: data => dispatch(afterCompareCityTemp(data))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(CityCouple);