import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import SettingsIcon from '@material-ui/icons/Settings';
import { Link } from "react-router-dom";
import NavigateNextIcon from '@material-ui/icons/NavigateNext';
import RotateLeftIcon from '@material-ui/icons/RotateLeft';
import { Container, CardHeader, IconButton, Tooltip, Button } from '@material-ui/core';
import CityCouple from './CityCouple';
import { withRouter } from "react-router";
import { connect } from 'react-redux';

const useStyles = makeStyles({
    bullet: {
        display: 'inline-block',
        margin: '0 2px',
        transform: 'scale(0.8)',
    },
    title: {
        fontSize: 18,
    },
    pos: {
        marginBottom: 12,
    },
})

const PlayPanel = props => {
    const {
        currentPlayStage,
        showNextPlay,
        retry,
        points
    } = props;
    const classes = useStyles();

    return (
        <Container maxWidth="lg">
            <Card className={`game-card`}>
                <CardHeader
                    action={
                        <>
                            <Tooltip title="Try Again" placement="left">
                                <IconButton aria-label="Try Again" onClick={retry}>
                                    <RotateLeftIcon />
                                </IconButton>
                            </Tooltip>
                            <Tooltip title="Settings" placement="left">
                                <Link to="/settings">
                                    <IconButton aria-label="settings">
                                        <SettingsIcon />
                                    </IconButton>
                                </Link>
                            </Tooltip>
                        </>
                    }
                    title={`Score: ${points.correct}/${points.overall}`}
                    subheader={''}
                />
                <CardContent>
                    <Typography className={classes.title} color="textSecondary" gutterBottom>
                        Which one is warmer?
                    </Typography>
                    <CityCouple />
                    {currentPlayStage === 'result' && <Button className="next-button" onClick={showNextPlay}>
                        Next
                        <NavigateNextIcon />
                    </Button>}
                </CardContent>
            </Card>
        </Container>
    );
}

const mapStateToProps = ({ gameReducer }) => {
    const { currentPlayStage, points } = gameReducer;
    return { currentPlayStage, points }
};

export default withRouter(connect(mapStateToProps)(PlayPanel));