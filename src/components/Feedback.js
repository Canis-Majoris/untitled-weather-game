import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import CheckCircleIcon from '@material-ui/icons/CheckCircle';
import ErrorIcon from '@material-ui/icons/Error';
import InfoIcon from '@material-ui/icons/Info';
import CloseIcon from '@material-ui/icons/Close';
import { amber, green, deepOrange, grey } from '@material-ui/core/colors';
import IconButton from '@material-ui/core/IconButton';
import Snackbar from '@material-ui/core/Snackbar';
import SnackbarContent from '@material-ui/core/SnackbarContent';
import WarningIcon from '@material-ui/icons/Warning';
import { connect } from 'react-redux';
import { setFeedbackStatus, setFeedbackMessage } from '../store/actions/game';

const variantIcon = {
    success: CheckCircleIcon,
    warning: WarningIcon,
    error: ErrorIcon,
    info: InfoIcon,
};

const useStyles1 = makeStyles(theme => ({
    success: {
        backgroundColor: green['A700'],
    },
    error: {
        backgroundColor: deepOrange['A700'],
    },
    info: {
        backgroundColor: grey[500],
    },
    warning: {
        backgroundColor: amber[700],
    },
    icon: {
        fontSize: 20,
    },
    iconVariant: {
        opacity: 0.9,
        marginRight: theme.spacing(1),
    },
    message: {
        display: 'flex',
        alignItems: 'center',
    },
}));


const MySnackbarContentWrapper = props => {
    const classes = useStyles1();
    const { className, message, onClose, variant, ...other } = props;
    const Icon = variantIcon[variant];

    return (
        <SnackbarContent
            className={clsx(classes[variant], className)}
            aria-describedby="client-snackbar"
            message={
                <span id="client-snackbar" className={classes.message}>
                    <Icon className={clsx(classes.icon, classes.iconVariant)} />
                    {message}
                </span>
            }
            action={[
                <IconButton key="close" aria-label="close" color="inherit" onClick={onClose}>
                    <CloseIcon className={classes.icon} />
                </IconButton>,
            ]}
            {...other}
        />
    );
}

MySnackbarContentWrapper.propTypes = {
    className: PropTypes.string,
    message: PropTypes.string,
    onClose: PropTypes.func,
    variant: PropTypes.oneOf(['error', 'info', 'success', 'warning']).isRequired,
};

const Feedback = props => {
    const {
        onSetFeedbackMessage,
        feedbackOpened,
        feedbackMessage
    } = props;

    const handleClose = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }

        // onSetFeedbackStatus(null)
        onSetFeedbackMessage(null)
    };

    return (
        <div>
            <Snackbar
                anchorOrigin={{
                    vertical: 'bottom',
                    horizontal: 'left',
                }}
                open={feedbackMessage !== null}
                autoHideDuration={5000}
                onClose={handleClose}
            >
                <MySnackbarContentWrapper
                    onClose={handleClose}
                    variant={feedbackOpened || 'info'}
                    message={feedbackMessage}
                />
            </Snackbar>
        </div>
    );
}

const mapStateToProps = ({ gameReducer }) => {
    const { feedbackOpened, feedbackMessage, points } = gameReducer;
    return { feedbackOpened, feedbackMessage, points }
};

const mapDispatchToProps = dispatch => {
    return {
        onSetFeedbackStatus: status => dispatch(setFeedbackStatus(status)),
        onSetFeedbackMessage: message => dispatch(setFeedbackMessage(message)),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Feedback);