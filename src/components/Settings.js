import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import { Link } from "react-router-dom";
import { Container, CardHeader, IconButton, Tooltip, FormControl, FormLabel, RadioGroup, FormControlLabel, Radio, Grid, Select, MenuItem, InputLabel, Divider } from '@material-ui/core';
import { setUnit, citiesNumberUpdate } from '../store/actions/game.js';
import { withRouter } from "react-router";
import { connect } from 'react-redux';
import Log from './Log.js';

const useStyles = makeStyles({
    bullet: {
        display: 'inline-block',
        margin: '0 2px',
        transform: 'scale(0.8)',
    },
    title: {
        fontSize: 14,
    },
    pos: {
        marginBottom: 12,
    },
})

const Settings = props => {
    const classes = useStyles();

    const {
        units,
        log,
        citiesNumber,
        onSetUnit,
        onCitiesNumberUpdate
    } = props;

    const [selectedUnits, setSelectedUnits] = useState(units);
    const [citiesN, setCitiesN] = useState(citiesNumber);

    useEffect(() => {
        setSelectedUnits(units);
    }, [units])

    const handleUnitsChange = e => {
        onSetUnit(e.target.value)

    }

    const handleCitiesNumberChange = e => {
        setCitiesN(e.target.value)
        onCitiesNumberUpdate(e.target.value)
    }

    return (
        <Container maxWidth="lg">
            <Card className={`game-card`}>
                <CardHeader
                    action={
                        <Tooltip title="Back to game" placement="left">
                            <Link to="/">
                                <IconButton aria-label="back">
                                    <ArrowBackIcon />
                                </IconButton>
                            </Link>
                        </Tooltip>
                    }
                    title="Settings"
                />
                <CardContent>
                    <div className="options-wrapper">
                        <Grid container direction="row" justify="flex-start" spacing={3}>
                            <Grid item xs={12}>
                                {units && (
                                    <FormControl component="fieldset" style={{marginRight: '1rem'}}>
                                        <FormLabel component="legend">Temperature Unit</FormLabel>
                                        <RadioGroup aria-label="gender" name="gender1" value={selectedUnits} onChange={handleUnitsChange}>
                                            <FormControlLabel value="metric" control={<Radio color="primary"/>} label="Celsius" />
                                            <FormControlLabel value="imperial" control={<Radio color="primary"/>} label="Fahrenheit" />
                                        </RadioGroup>
                                    </FormControl>
                                )}
                                <FormControl component="fieldset" style={{maxWidth: 220}} style={{marginRight: '1rem'}}>
                                    <FormLabel component="legend">Cities to Choose from</FormLabel>
                                    <Select
                                        value={citiesN}
                                        onChange={handleCitiesNumberChange}
                                        >
                                        <MenuItem value={2}>2</MenuItem>
                                        <MenuItem value={3}>3</MenuItem>
                                        <MenuItem value={4}>4</MenuItem>
                                        <MenuItem value={5}>5</MenuItem>
                                    </Select>
                                </FormControl>
                            </Grid> 
                        </Grid>
                    </div>
                    <Divider style={{marginBottom: '1rem'}}/>
                    <Log log={log} />
                </CardContent>
            </Card>
        </Container>
    );
}


const mapStateToProps = ({ gameReducer }) => {
    const { units, log, citiesNumber } = gameReducer;
    return { units, log, citiesNumber }
};
const mapDispatchToProps = dispatch => {
    return {
        onSetUnit: unit => dispatch(setUnit(unit)),
        onCitiesNumberUpdate: number => dispatch(citiesNumberUpdate(number))
    }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Settings));