import {
    UPDATE_LOG,
    FETCH_CITY_PENDING,
    FETCH_CITY_SUCCESS,
    SET_PLAY_STATUS,
    SET_PLAY_STAGE,
    SET_CORRECT_ANSWER_INDEX,
    SET_UNIT,
    UPDATE_POINTS,
    SET_FEEDBACK_STATUS,
    SET_FEEDBACK_MESSAGE,
    AFTER_COMPARE_CITY_TEMP,
    CITIES_NUMBER_UPDATE
} from '../../constants/ActionTypes';
import moment from 'moment';
import { defaultBoxNumber } from '../../constants/misc';

const initialState = {
    units: localStorage.getItem('weather__units') || 'metric',
    points: JSON.parse(localStorage.getItem('weather__points')) || {
        overall: 0,
        correct: 0
    },
    log: JSON.parse(localStorage.getItem('weather__log')) || [],
    cities: [],
    citiesNumber: +localStorage.getItem('cities__number') || defaultBoxNumber,
    correctAnswerIndex: -1,
    wrongAnswerIndex: -1,
    currentPlayStatus: null,
    currentPlayStage: 'match',
    fetchCitiesPending: false,
    feedbackOpened: null,
    feedbackMessage: null
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_CITY_SUCCESS: {
            return {
                ...state,
                fetchCitiesPending: false,
                cities: action.cities,
            }
        }
        case SET_PLAY_STATUS: {
            return {
                ...state,
                currentPlayStatus: action.status,
            }
        }
        case SET_PLAY_STAGE: {
            return {
                ...state,
                currentPlayStage: action.stage,
            }
        }
        case SET_CORRECT_ANSWER_INDEX: {
            return {
                ...state,
                correctAnswerIndex: action.index,
            }
        }
        case FETCH_CITY_PENDING: {
            return {
                ...state,
                fetchCitiesPending: true,
            }
        }
        case SET_UNIT: {
            localStorage.setItem('weather__units', action.units)
            
            return {
                ...state,
                units: action.units,
            }
        }

        case CITIES_NUMBER_UPDATE: {
            localStorage.setItem('cities__number', action.number)
            
            return {
                ...state,
                citiesNumber: action.number,
            }
        }
        case UPDATE_LOG: {
            return {
                ...state,
                log: action.log,
            }
        }
        case UPDATE_POINTS: {
            return {
                ...state,
                points: action.points,
            }
        }
        case SET_FEEDBACK_STATUS: {
            return {
                ...state,
                feedbackOpened: action.status,
            }
        }
        case SET_FEEDBACK_MESSAGE: {
            return {
                ...state,
                feedbackMessage: action.message,
            }
        }
        case AFTER_COMPARE_CITY_TEMP: {
            const result = action.data.currentIndex === action.data.index;
            const newLog = [{
                correctAnswerIndex: action.data.currentIndex,
                wrongAnswerIndex: !result ? action.data.index : -1,
                cities: state.cities,
                result: result,
                date: moment().format('LLL')
            }, ...state.log]
            
            let { overall, correct } = state.points;
            overall != null && overall++;
            correct != null && result && correct++;
            
            const newPoints = {
                overall: overall,
                correct: correct,
            }
            
            localStorage.setItem('weather__log', JSON.stringify(newLog))
            localStorage.setItem('weather__points', JSON.stringify(newPoints))
            
            return {
                ...state,
                log: newLog,
                points: newPoints,
                feedbackMessage: action.message,
            }
        }
        default:
            return state
    }
}

export default reducer;