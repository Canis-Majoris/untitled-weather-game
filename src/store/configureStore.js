import { combineReducers, createStore, applyMiddleware, compose } from 'redux';
import gameReducer from './reducers/game';
import thunk from 'redux-thunk';

const allReducers = combineReducers({
    gameReducer
})

const rootReducer = (state, action) => {
    return allReducers(state, action);
}

const composeEnhancer = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export const store = createStore(
    rootReducer,
    composeEnhancer(applyMiddleware(thunk)),
);

const configureStore = () => {
    return store;
}

export default configureStore;