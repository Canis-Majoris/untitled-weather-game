import {
    UPDATE_LOG,
    UPDATE_POINTS,
    FETCH_CITY_PENDING,
    FETCH_CITY_ERROR,
    FETCH_CITY_SUCCESS,
    SET_PLAY_STATUS,
    SET_PLAY_STAGE,
    SET_CORRECT_ANSWER_INDEX,
    SET_UNIT,
    SET_FEEDBACK_STATUS,
    SET_FEEDBACK_MESSAGE,
    AFTER_COMPARE_CITY_TEMP,
    CITIES_NUMBER_UPDATE
} from '../../constants/ActionTypes';
import config from '../../config';

const fetchCityPending = () => {
    return {
        type: FETCH_CITY_PENDING
    }
}

const fetchCitySuccess = (cities) => {
    return {
        type: FETCH_CITY_SUCCESS,
        cities
    }
}

const fetchCityError = (error) => {
    return {
        type: FETCH_CITY_ERROR,
        error: error
    }
}

export const fetchCityData = (cityArr) => {
    return async (dispatch) => {
        let cityPromiseArr = [];
        let cityDataArr = []
        dispatch(fetchCityPending());
        cityPromiseArr = cityArr.map((c, i) => 
            fetch(`${config.api.url}/weather?APPID=${config.api.key}&id=${c.id}&units=metric`)
                .then(res => res.json())
                .then(res => {
                    if (res.error) {
                        throw (res.error);
                    }
                    cityDataArr[i] = res
                })
                .catch(error => {
                    dispatch(fetchCityError(error));
                })
        );

        Promise.all(cityPromiseArr).then(() => {
            dispatch(fetchCitySuccess(cityDataArr));
        });
    }
}

export const setPlayStatus = status => {
    return {
        type: SET_PLAY_STATUS,
        status
    }
}

export const setPlayStage = stage => {
    return {
        type: SET_PLAY_STAGE,
        stage
    }
}

export const setCorrectAnswerIndex = index => {
    return {
        type: SET_CORRECT_ANSWER_INDEX,
        index
    }
}

export const setUnit = units => {
    return {
        type: SET_UNIT,
        units
    }
}

export const citiesNumberUpdate = number => {
    return {
        type: CITIES_NUMBER_UPDATE,
        number
    }  
}

export const updateLog = log => {
    return {
        type: UPDATE_LOG,
        log
    }
}

export const updatePoints = points => {
    return {
        type: UPDATE_POINTS,
        points
    }
}

export const setFeedbackStatus = status => {
    return {
        type: SET_FEEDBACK_STATUS,
        status
    }
}

export const setFeedbackMessage = message => {
    return {
        type: SET_FEEDBACK_MESSAGE,
        message
    }
}

export const afterCompareCityTemp = data => {
    return {
        type: AFTER_COMPARE_CITY_TEMP,
        data
    }
}





