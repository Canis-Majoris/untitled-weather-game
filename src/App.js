import React from 'react';
import Game from './components/Game';
import './assets/scss/App.scss'

function App() {
  return (
    <Game />
  );
}

export default App;
