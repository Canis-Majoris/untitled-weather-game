// Game
export const FETCH_CITY_DATA = 'fetch_city_data';
export const UPDATE_LOG = 'add_to_log';
export const SET_UNITS = 'set_units';
export const FETCH_CITY_PENDING = 'fetch_city_pending';
export const FETCH_CITY_ERROR = 'fetch_city_error';
export const FETCH_CITY_SUCCESS = 'fetch_city_success';
export const SET_PLAY_STATUS = 'set_play_status';
export const SET_PLAY_STAGE = 'set_play_stage';
export const SET_CORRECT_ANSWER_INDEX = 'set_correct_answer_index';
export const SET_UNIT = 'set_unit';
export const UPDATE_POINTS = 'set_points';
export const SET_FEEDBACK_STATUS = 'set_feedback_status';
export const SET_FEEDBACK_MESSAGE = 'set_feedback_message';
export const AFTER_COMPARE_CITY_TEMP = 'after_compare_city_temp';
export const CITIES_NUMBER_UPDATE = 'cities_number_update'




